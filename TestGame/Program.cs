﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseGame
{
    class Program
    {
        static void Main(string[] args)
        {
            using (BaseGameCore MyBaseGameCore = new BaseGameCore())
            {
                MyBaseGameCore.Initialize();
            }
        }

    }
}
