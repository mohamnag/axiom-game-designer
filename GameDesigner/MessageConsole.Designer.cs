﻿namespace GameDesigner
{
    partial class MessageConsole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txbMessages = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // txbMessages
            // 
            this.txbMessages.BackColor = System.Drawing.SystemColors.WindowText;
            this.txbMessages.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txbMessages.Cursor = System.Windows.Forms.Cursors.Default;
            this.txbMessages.DetectUrls = false;
            this.txbMessages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txbMessages.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txbMessages.Location = new System.Drawing.Point(0, 0);
            this.txbMessages.Name = "txbMessages";
            this.txbMessages.ReadOnly = true;
            this.txbMessages.Size = new System.Drawing.Size(609, 101);
            this.txbMessages.TabIndex = 0;
            this.txbMessages.Text = "";
            this.txbMessages.WordWrap = false;
            this.txbMessages.TextChanged += new System.EventHandler(this.txbMessages_TextChanged);
            // 
            // MessageConsole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 101);
            this.ControlBox = false;
            this.Controls.Add(this.txbMessages);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MessageConsole";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "MessageConsole";
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.RichTextBox txbMessages;

    }
}