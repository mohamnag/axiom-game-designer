﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Axiom.Graphics.Collections;
using Axiom.Configuration;

namespace GameDesigner
{
    public partial class ConfigOptionCollectionEditorForm : Form
    {
        public ConfigOptionCollectionEditorForm(ConfigOptionCollection Collection)
        {
            InitializeComponent();

            lstOptions.Items.Clear();

            foreach (ConfigOption cfg in Collection)
                lstOptions.Items.Add(cfg);
        }

        private void lstOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboOptionValues.SelectedIndexChanged -= cboOptionValues_SelectedIndexChanged;

            cboOptionValues.Items.Clear();

            int selIn = 0;

            foreach (string val in ((ConfigOption)lstOptions.SelectedItem).PossibleValues.Values)
            {
                cboOptionValues.Items.Add(val);
                if (val == ((ConfigOption)lstOptions.SelectedItem).Value)
                    selIn = cboOptionValues.Items.Count - 1;
            }

            cboOptionValues.SelectedIndex = selIn;

            cboOptionValues.SelectedIndexChanged += cboOptionValues_SelectedIndexChanged;
        }

        private void cboOptionValues_SelectedIndexChanged(object sender, EventArgs e)
        {
            ((ConfigOption)lstOptions.SelectedItem).Value = (string)cboOptionValues.SelectedItem;

            lstOptions.SelectedIndexChanged -= lstOptions_SelectedIndexChanged;
            lstOptions.Items[lstOptions.SelectedIndex] = lstOptions.SelectedItem;
            lstOptions.SelectedIndexChanged += lstOptions_SelectedIndexChanged;
        }
    }
}
