﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GameDesigner
{
    public partial class AddSceneNode : Form
    {
        public string NodeName
        {
            get
            {
                return txbNodeName.Text;
            }
        }

        public AddSceneNode()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txbNodeName.Text))
            {
                MessageBox.Show("Node Name can not be empty.");
            }
            else
                this.DialogResult = DialogResult.OK;
        }
    }
}
