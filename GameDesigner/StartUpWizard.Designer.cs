﻿namespace GameDesigner
{
    partial class StartUpWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartUpWizard));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnFinish = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.wzContainer = new System.Windows.Forms.Panel();
            this.wzStepCoreSetup = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.RenderSystemPropertiesBtn = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.lblScenemanagerDescription = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbScenemanagerName = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbScenemanagerType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbRenderSystem = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.wzStepEngingeProjectConfig = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txbProjectName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.wzStepResources = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.WorlGeometryChb = new System.Windows.Forms.CheckBox();
            this.EditWorldGeometryBtn = new System.Windows.Forms.Button();
            this.ResourceEditor = new System.Windows.Forms.PropertyGrid();
            this.label15 = new System.Windows.Forms.Label();
            this.worldGeometryOD = new System.Windows.Forms.OpenFileDialog();
            this.label8 = new System.Windows.Forms.Label();
            this.odProjectAdress = new GameDesigner.openDialog();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.wzContainer.SuspendLayout();
            this.wzStepCoreSetup.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.wzStepEngingeProjectConfig.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.wzStepResources.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Info;
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(699, 79);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 9);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(60, 60);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.InfoText;
            this.label1.Location = new System.Drawing.Point(78, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(259, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Game Designer Startup Wizard";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnBack);
            this.panel2.Controls.Add(this.btnCancel);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.btnFinish);
            this.panel2.Controls.Add(this.btnNext);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 462);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(699, 43);
            this.panel2.TabIndex = 1;
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(403, 8);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(75, 23);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(612, 8);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(699, 1);
            this.panel3.TabIndex = 0;
            // 
            // btnFinish
            // 
            this.btnFinish.Location = new System.Drawing.Point(484, 8);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(75, 23);
            this.btnFinish.TabIndex = 4;
            this.btnFinish.Text = "Finish";
            this.btnFinish.UseVisualStyleBackColor = true;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(484, 8);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 2;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // wzContainer
            // 
            this.wzContainer.Controls.Add(this.wzStepEngingeProjectConfig);
            this.wzContainer.Controls.Add(this.wzStepResources);
            this.wzContainer.Controls.Add(this.wzStepCoreSetup);
            this.wzContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wzContainer.Location = new System.Drawing.Point(0, 79);
            this.wzContainer.Name = "wzContainer";
            this.wzContainer.Size = new System.Drawing.Size(699, 383);
            this.wzContainer.TabIndex = 3;
            // 
            // wzStepCoreSetup
            // 
            this.wzStepCoreSetup.Controls.Add(this.groupBox1);
            this.wzStepCoreSetup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wzStepCoreSetup.Location = new System.Drawing.Point(0, 0);
            this.wzStepCoreSetup.Name = "wzStepCoreSetup";
            this.wzStepCoreSetup.Size = new System.Drawing.Size(699, 383);
            this.wzStepCoreSetup.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.RenderSystemPropertiesBtn);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.lblScenemanagerDescription);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.cmbScenemanagerName);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cmbScenemanagerType);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cmbRenderSystem);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(16, 18);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(666, 346);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Axiom core config";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 270);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Scene Manager Description:";
            // 
            // RenderSystemPropertiesBtn
            // 
            this.RenderSystemPropertiesBtn.Enabled = false;
            this.RenderSystemPropertiesBtn.Location = new System.Drawing.Point(246, 51);
            this.RenderSystemPropertiesBtn.Name = "RenderSystemPropertiesBtn";
            this.RenderSystemPropertiesBtn.Size = new System.Drawing.Size(75, 23);
            this.RenderSystemPropertiesBtn.TabIndex = 14;
            this.RenderSystemPropertiesBtn.Text = "Properties";
            this.RenderSystemPropertiesBtn.UseVisualStyleBackColor = true;
            this.RenderSystemPropertiesBtn.Click += new System.EventHandler(this.RenderSystemPropertiesBtn_Click);
            // 
            // label13
            // 
            this.label13.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.label13.Location = new System.Drawing.Point(326, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(328, 67);
            this.label13.TabIndex = 12;
            this.label13.Text = "Select which render system will be used for your application. Select the full scr" +
                "een option and video mode for this render system.";
            // 
            // lblScenemanagerDescription
            // 
            this.lblScenemanagerDescription.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.lblScenemanagerDescription.Location = new System.Drawing.Point(324, 270);
            this.lblScenemanagerDescription.Name = "lblScenemanagerDescription";
            this.lblScenemanagerDescription.Size = new System.Drawing.Size(329, 68);
            this.lblScenemanagerDescription.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 146);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Scene Manager Name:";
            // 
            // cmbScenemanagerName
            // 
            this.cmbScenemanagerName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbScenemanagerName.Enabled = false;
            this.cmbScenemanagerName.FormattingEnabled = true;
            this.cmbScenemanagerName.Location = new System.Drawing.Point(128, 143);
            this.cmbScenemanagerName.Name = "cmbScenemanagerName";
            this.cmbScenemanagerName.Size = new System.Drawing.Size(192, 21);
            this.cmbScenemanagerName.TabIndex = 6;
            this.cmbScenemanagerName.SelectedIndexChanged += new System.EventHandler(this.cmbScenemanagerName_SelectedIndexChanged);
            this.cmbScenemanagerName.DropDown += new System.EventHandler(this.cmbScenemanagerName_DropDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Scene Manager Type: *";
            // 
            // cmbScenemanagerType
            // 
            this.cmbScenemanagerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbScenemanagerType.FormattingEnabled = true;
            this.cmbScenemanagerType.Location = new System.Drawing.Point(128, 117);
            this.cmbScenemanagerType.Name = "cmbScenemanagerType";
            this.cmbScenemanagerType.Size = new System.Drawing.Size(192, 21);
            this.cmbScenemanagerType.TabIndex = 4;
            this.cmbScenemanagerType.SelectedIndexChanged += new System.EventHandler(this.cmbScenemanagerType_SelectedIndexChanged);
            this.cmbScenemanagerType.DropDown += new System.EventHandler(this.cmbScenemanagerType_DropDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Render System: *";
            // 
            // cmbRenderSystem
            // 
            this.cmbRenderSystem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRenderSystem.FormattingEnabled = true;
            this.cmbRenderSystem.Items.AddRange(new object[] {
            "DiretX 9",
            "OpenGL",
            "XNA"});
            this.cmbRenderSystem.Location = new System.Drawing.Point(128, 22);
            this.cmbRenderSystem.Name = "cmbRenderSystem";
            this.cmbRenderSystem.Size = new System.Drawing.Size(192, 21);
            this.cmbRenderSystem.TabIndex = 2;
            this.cmbRenderSystem.SelectedIndexChanged += new System.EventHandler(this.cmbRenderSystem_SelectedIndexChanged);
            this.cmbRenderSystem.DropDown += new System.EventHandler(this.cmbRenderSystem_DropDown);
            // 
            // label14
            // 
            this.label14.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.label14.Location = new System.Drawing.Point(326, 117);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(328, 143);
            this.label14.TabIndex = 13;
            this.label14.Text = resources.GetString("label14.Text");
            // 
            // wzStepEngingeProjectConfig
            // 
            this.wzStepEngingeProjectConfig.Controls.Add(this.label32);
            this.wzStepEngingeProjectConfig.Controls.Add(this.groupBox3);
            this.wzStepEngingeProjectConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wzStepEngingeProjectConfig.Location = new System.Drawing.Point(0, 0);
            this.wzStepEngingeProjectConfig.Name = "wzStepEngingeProjectConfig";
            this.wzStepEngingeProjectConfig.Size = new System.Drawing.Size(699, 383);
            this.wzStepEngingeProjectConfig.TabIndex = 4;
            // 
            // label32
            // 
            this.label32.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.label32.Location = new System.Drawing.Point(21, 320);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(648, 51);
            this.label32.TabIndex = 11;
            this.label32.Text = "This is just a startup wizard which helps you prepare base needs for a project. Y" +
                "ou can redo or change almost all options available in this wizard, later during " +
                "the edit of project.";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.txbProjectName);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.odProjectAdress);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(16, 18);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(665, 138);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Project settings";
            // 
            // label12
            // 
            this.label12.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.label12.Location = new System.Drawing.Point(326, 98);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(328, 33);
            this.label12.TabIndex = 10;
            this.label12.Text = "Root address is the folder that all project\'s files containing resources and engi" +
                "ne description file will be saved to.";
            // 
            // label11
            // 
            this.label11.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.label11.Location = new System.Drawing.Point(326, 41);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(328, 33);
            this.label11.TabIndex = 10;
            this.label11.Text = "Project name is used to save files that should be transfered to Axiom engine gene" +
                "rator.";
            // 
            // txbProjectName
            // 
            this.txbProjectName.Location = new System.Drawing.Point(29, 41);
            this.txbProjectName.Name = "txbProjectName";
            this.txbProjectName.Size = new System.Drawing.Size(291, 20);
            this.txbProjectName.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Project name: *";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Root address: *";
            // 
            // wzStepResources
            // 
            this.wzStepResources.Controls.Add(this.groupBox2);
            this.wzStepResources.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wzStepResources.Location = new System.Drawing.Point(0, 0);
            this.wzStepResources.Name = "wzStepResources";
            this.wzStepResources.Size = new System.Drawing.Size(699, 383);
            this.wzStepResources.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.WorlGeometryChb);
            this.groupBox2.Controls.Add(this.EditWorldGeometryBtn);
            this.groupBox2.Controls.Add(this.ResourceEditor);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(16, 18);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(665, 346);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Resources";
            // 
            // WorlGeometryChb
            // 
            this.WorlGeometryChb.AutoSize = true;
            this.WorlGeometryChb.Enabled = false;
            this.WorlGeometryChb.Location = new System.Drawing.Point(327, 321);
            this.WorlGeometryChb.Name = "WorlGeometryChb";
            this.WorlGeometryChb.Size = new System.Drawing.Size(124, 17);
            this.WorlGeometryChb.TabIndex = 18;
            this.WorlGeometryChb.Text = "Use World Geometry";
            this.WorlGeometryChb.UseVisualStyleBackColor = true;
            // 
            // EditWorldGeometryBtn
            // 
            this.EditWorldGeometryBtn.Enabled = false;
            this.EditWorldGeometryBtn.Location = new System.Drawing.Point(519, 317);
            this.EditWorldGeometryBtn.Name = "EditWorldGeometryBtn";
            this.EditWorldGeometryBtn.Size = new System.Drawing.Size(134, 23);
            this.EditWorldGeometryBtn.TabIndex = 17;
            this.EditWorldGeometryBtn.Text = "Edit World Geometry";
            this.EditWorldGeometryBtn.UseVisualStyleBackColor = true;
            this.EditWorldGeometryBtn.Click += new System.EventHandler(this.EditWorldGeometryBtn_Click);
            // 
            // ResourceEditor
            // 
            this.ResourceEditor.Location = new System.Drawing.Point(9, 19);
            this.ResourceEditor.Name = "ResourceEditor";
            this.ResourceEditor.Size = new System.Drawing.Size(312, 321);
            this.ResourceEditor.TabIndex = 14;
            this.ResourceEditor.ToolbarVisible = false;
            // 
            // label15
            // 
            this.label15.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.label15.Location = new System.Drawing.Point(325, 41);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(328, 90);
            this.label15.TabIndex = 11;
            this.label15.Text = "Please select the resources that you want to add to project. All of this resource" +
                "s will be copied to the project\'s folder under subfolders with their group name." +
                " Source files will not be manipulated.";
            // 
            // worldGeometryOD
            // 
            this.worldGeometryOD.Filter = "XML files (*.xml)|*.txt|All files (*.*)|*.*";
            this.worldGeometryOD.Title = "Load geometry descriptor";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(79, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(351, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "This wizard will guide you through the first steps needed for a new project";
            // 
            // odProjectAdress
            // 
            this.odProjectAdress.Location = new System.Drawing.Point(29, 98);
            this.odProjectAdress.Name = "odProjectAdress";
            this.odProjectAdress.ReadOnly = true;
            this.odProjectAdress.Size = new System.Drawing.Size(291, 23);
            this.odProjectAdress.TabIndex = 7;
            // 
            // StartUpWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 505);
            this.Controls.Add(this.wzContainer);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "StartUpWizard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "StartUpWizard";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.wzContainer.ResumeLayout(false);
            this.wzStepCoreSetup.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.wzStepEngingeProjectConfig.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.wzStepResources.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel wzContainer;
        private System.Windows.Forms.OpenFileDialog worldGeometryOD;
        private System.Windows.Forms.Panel wzStepResources;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel wzStepEngingeProjectConfig;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private openDialog odProjectAdress;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txbProjectName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnFinish;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel wzStepCoreSetup;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblScenemanagerDescription;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbScenemanagerName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbScenemanagerType;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbRenderSystem;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button RenderSystemPropertiesBtn;
        private System.Windows.Forms.PropertyGrid ResourceEditor;
        private System.Windows.Forms.CheckBox WorlGeometryChb;
        private System.Windows.Forms.Button EditWorldGeometryBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
    }
}