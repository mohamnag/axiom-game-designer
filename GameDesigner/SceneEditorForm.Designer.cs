﻿namespace GameDesigner
{
    partial class SceneEditorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Render System");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Scene Hierarchy");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Lights");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Configs");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Terrain Nodes");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Terrain", new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode5});
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Static Geometry");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Cameras");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Resources");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SceneEditorForm));
            this.RenderSystemRightClickMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.switchRenderSystemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SceneNodeRightClickMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newSubSceneNodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.AddMovableObjectMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.attachedObjectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thisAttachedObjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sceneNodeAndObjectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LightsRightClickMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.refreshLightsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.TerrainConfigRightClickMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.applyTerrainConfigChangesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CamerasRightClickMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.refreshCamerasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ResourcesRightClickMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.reinitializeAllResourcesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.HierarchyTreeView = new System.Windows.Forms.TreeView();
            this.TreeImageList = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.PropertyViewer = new System.Windows.Forms.PropertyGrid();
            this.panel2 = new System.Windows.Forms.Panel();
            this.ObjectTypeLbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.MovableObjectRightClickMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.reloadEngineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RenderSystemRightClickMenu.SuspendLayout();
            this.SceneNodeRightClickMenu.SuspendLayout();
            this.LightsRightClickMenu.SuspendLayout();
            this.TerrainConfigRightClickMenu.SuspendLayout();
            this.CamerasRightClickMenu.SuspendLayout();
            this.ResourcesRightClickMenu.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.MovableObjectRightClickMenu.SuspendLayout();
            this.MainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // RenderSystemRightClickMenu
            // 
            this.RenderSystemRightClickMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.switchRenderSystemToolStripMenuItem});
            this.RenderSystemRightClickMenu.Name = "RenderSystemRightClickMenu";
            this.RenderSystemRightClickMenu.Size = new System.Drawing.Size(190, 26);
            this.RenderSystemRightClickMenu.Opening += new System.ComponentModel.CancelEventHandler(this.RenderSystemRightClickMenu_Opening);
            // 
            // switchRenderSystemToolStripMenuItem
            // 
            this.switchRenderSystemToolStripMenuItem.Name = "switchRenderSystemToolStripMenuItem";
            this.switchRenderSystemToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.switchRenderSystemToolStripMenuItem.Text = "Switch RenderSystem";
            // 
            // SceneNodeRightClickMenu
            // 
            this.SceneNodeRightClickMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.toolStripMenuItem1,
            this.refreshToolStripMenuItem});
            this.SceneNodeRightClickMenu.Name = "SceneNodeRightClickMenu";
            this.SceneNodeRightClickMenu.Size = new System.Drawing.Size(124, 76);
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newSubSceneNodeToolStripMenuItem,
            this.toolStripSeparator1,
            this.AddMovableObjectMenuItem});
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.addToolStripMenuItem.Text = "Add";
            this.addToolStripMenuItem.DropDownOpening += new System.EventHandler(this.addToolStripMenuItem_DropDownOpening);
            // 
            // newSubSceneNodeToolStripMenuItem
            // 
            this.newSubSceneNodeToolStripMenuItem.Name = "newSubSceneNodeToolStripMenuItem";
            this.newSubSceneNodeToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.newSubSceneNodeToolStripMenuItem.Text = "SceneNode";
            this.newSubSceneNodeToolStripMenuItem.Click += new System.EventHandler(this.newSubSceneNodeToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(154, 6);
            // 
            // AddMovableObjectMenuItem
            // 
            this.AddMovableObjectMenuItem.Name = "AddMovableObjectMenuItem";
            this.AddMovableObjectMenuItem.Size = new System.Drawing.Size(157, 22);
            this.AddMovableObjectMenuItem.Text = "MovableObject";
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.attachedObjectsToolStripMenuItem,
            this.thisAttachedObjectToolStripMenuItem,
            this.sceneNodeAndObjectsToolStripMenuItem});
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.DropDownOpening += new System.EventHandler(this.deleteToolStripMenuItem_DropDownOpening);
            // 
            // attachedObjectsToolStripMenuItem
            // 
            this.attachedObjectsToolStripMenuItem.Name = "attachedObjectsToolStripMenuItem";
            this.attachedObjectsToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.attachedObjectsToolStripMenuItem.Text = "All attached objects";
            this.attachedObjectsToolStripMenuItem.Click += new System.EventHandler(this.attachedObjectsToolStripMenuItem_Click);
            // 
            // thisAttachedObjectToolStripMenuItem
            // 
            this.thisAttachedObjectToolStripMenuItem.Name = "thisAttachedObjectToolStripMenuItem";
            this.thisAttachedObjectToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.thisAttachedObjectToolStripMenuItem.Text = "This attached object";
            // 
            // sceneNodeAndObjectsToolStripMenuItem
            // 
            this.sceneNodeAndObjectsToolStripMenuItem.Name = "sceneNodeAndObjectsToolStripMenuItem";
            this.sceneNodeAndObjectsToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.sceneNodeAndObjectsToolStripMenuItem.Text = "SceneNode and objects";
            this.sceneNodeAndObjectsToolStripMenuItem.Click += new System.EventHandler(this.sceneNodeAndObjectsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(120, 6);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // LightsRightClickMenu
            // 
            this.LightsRightClickMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshLightsToolStripMenuItem1});
            this.LightsRightClickMenu.Name = "LightsRightClickMenu";
            this.LightsRightClickMenu.Size = new System.Drawing.Size(124, 26);
            // 
            // refreshLightsToolStripMenuItem1
            // 
            this.refreshLightsToolStripMenuItem1.Name = "refreshLightsToolStripMenuItem1";
            this.refreshLightsToolStripMenuItem1.Size = new System.Drawing.Size(123, 22);
            this.refreshLightsToolStripMenuItem1.Text = "Refresh";
            this.refreshLightsToolStripMenuItem1.Click += new System.EventHandler(this.refreshLightsToolStripMenuItem1_Click);
            // 
            // TerrainConfigRightClickMenu
            // 
            this.TerrainConfigRightClickMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.applyTerrainConfigChangesToolStripMenuItem});
            this.TerrainConfigRightClickMenu.Name = "TerrainConfigRightClickMenu";
            this.TerrainConfigRightClickMenu.Size = new System.Drawing.Size(195, 26);
            // 
            // applyTerrainConfigChangesToolStripMenuItem
            // 
            this.applyTerrainConfigChangesToolStripMenuItem.Name = "applyTerrainConfigChangesToolStripMenuItem";
            this.applyTerrainConfigChangesToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.applyTerrainConfigChangesToolStripMenuItem.Text = "Apply Terrain Changes";
            this.applyTerrainConfigChangesToolStripMenuItem.Click += new System.EventHandler(this.applyTerrainConfigChangesToolStripMenuItem_Click);
            // 
            // CamerasRightClickMenu
            // 
            this.CamerasRightClickMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshCamerasToolStripMenuItem1});
            this.CamerasRightClickMenu.Name = "CamerasRightClickMenu";
            this.CamerasRightClickMenu.Size = new System.Drawing.Size(124, 26);
            // 
            // refreshCamerasToolStripMenuItem1
            // 
            this.refreshCamerasToolStripMenuItem1.Name = "refreshCamerasToolStripMenuItem1";
            this.refreshCamerasToolStripMenuItem1.Size = new System.Drawing.Size(123, 22);
            this.refreshCamerasToolStripMenuItem1.Text = "Refresh";
            this.refreshCamerasToolStripMenuItem1.Click += new System.EventHandler(this.refreshCamerasToolStripMenuItem1_Click);
            // 
            // ResourcesRightClickMenu
            // 
            this.ResourcesRightClickMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reinitializeAllResourcesToolStripMenuItem});
            this.ResourcesRightClickMenu.Name = "ResourcesRightClickMenu";
            this.ResourcesRightClickMenu.Size = new System.Drawing.Size(153, 26);
            // 
            // reinitializeAllResourcesToolStripMenuItem
            // 
            this.reinitializeAllResourcesToolStripMenuItem.Name = "reinitializeAllResourcesToolStripMenuItem";
            this.reinitializeAllResourcesToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.reinitializeAllResourcesToolStripMenuItem.Text = "Re-initialize all";
            this.reinitializeAllResourcesToolStripMenuItem.Click += new System.EventHandler(this.reinitializeAllResourcesToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.HierarchyTreeView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(284, 542);
            this.splitContainer1.SplitterDistance = 239;
            this.splitContainer1.TabIndex = 2;
            // 
            // HierarchyTreeView
            // 
            this.HierarchyTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HierarchyTreeView.HideSelection = false;
            this.HierarchyTreeView.ImageIndex = 0;
            this.HierarchyTreeView.ImageList = this.TreeImageList;
            this.HierarchyTreeView.Indent = 19;
            this.HierarchyTreeView.Location = new System.Drawing.Point(0, 0);
            this.HierarchyTreeView.Name = "HierarchyTreeView";
            treeNode1.ContextMenuStrip = this.RenderSystemRightClickMenu;
            treeNode1.ImageKey = "Basic";
            treeNode1.Name = "RenderSystemTree";
            treeNode1.SelectedImageKey = "Basic";
            treeNode1.Text = "Render System";
            treeNode2.ContextMenuStrip = this.SceneNodeRightClickMenu;
            treeNode2.ImageKey = "SceneNode";
            treeNode2.Name = "SceneNodesTree";
            treeNode2.SelectedImageKey = "SceneNode";
            treeNode2.Text = "Scene Hierarchy";
            treeNode2.ToolTipText = "Here the relation between different Objects is defined with the use of SceneNodes" +
                ".";
            treeNode3.ContextMenuStrip = this.LightsRightClickMenu;
            treeNode3.ImageKey = "Light";
            treeNode3.Name = "LightsTree";
            treeNode3.SelectedImageKey = "Light";
            treeNode3.Text = "Lights";
            treeNode3.ToolTipText = "Here you will see the whole lights defined. They can be attached to Scene Hierarc" +
                "hy or not (for example for future use).";
            treeNode4.ContextMenuStrip = this.TerrainConfigRightClickMenu;
            treeNode4.ImageKey = "Config";
            treeNode4.Name = "TerrainConfig";
            treeNode4.SelectedImageKey = "Config";
            treeNode4.Text = "Configs";
            treeNode5.ImageKey = "Terrain";
            treeNode5.Name = "TerrainNodesTree";
            treeNode5.SelectedImageKey = "Terrain";
            treeNode5.Text = "Terrain Nodes";
            treeNode6.ImageKey = "Terrain";
            treeNode6.Name = "TerrainTree";
            treeNode6.SelectedImageKey = "Terrain";
            treeNode6.Text = "Terrain";
            treeNode6.ToolTipText = resources.GetString("treeNode6.ToolTipText");
            treeNode7.ImageKey = "Static";
            treeNode7.Name = "StaticGeometryTree";
            treeNode7.SelectedImageKey = "Static";
            treeNode7.Text = "Static Geometry";
            treeNode7.ToolTipText = "Static Geometry is used to define objects that will and can not move later.";
            treeNode8.ContextMenuStrip = this.CamerasRightClickMenu;
            treeNode8.ImageKey = "Camera";
            treeNode8.Name = "CamerasTree";
            treeNode8.SelectedImageKey = "Camera";
            treeNode8.Text = "Cameras";
            treeNode8.ToolTipText = "Here you will see the whole cameras defined. They can be attached to Scene Hierar" +
                "chy or not (for example for future use).";
            treeNode9.ContextMenuStrip = this.ResourcesRightClickMenu;
            treeNode9.ImageKey = "Resource";
            treeNode9.Name = "ResourcesTree";
            treeNode9.SelectedImageKey = "Resource";
            treeNode9.Text = "Resources";
            this.HierarchyTreeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode6,
            treeNode7,
            treeNode8,
            treeNode9});
            this.HierarchyTreeView.SelectedImageIndex = 0;
            this.HierarchyTreeView.ShowNodeToolTips = true;
            this.HierarchyTreeView.Size = new System.Drawing.Size(284, 239);
            this.HierarchyTreeView.TabIndex = 1;
            this.HierarchyTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.HierarchyTreeView_AfterSelect);
            this.HierarchyTreeView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.HierarchyTreeView_MouseDown);
            // 
            // TreeImageList
            // 
            this.TreeImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("TreeImageList.ImageStream")));
            this.TreeImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.TreeImageList.Images.SetKeyName(0, "Entity");
            this.TreeImageList.Images.SetKeyName(1, "SceneNode");
            this.TreeImageList.Images.SetKeyName(2, "Light");
            this.TreeImageList.Images.SetKeyName(3, "Terrain");
            this.TreeImageList.Images.SetKeyName(4, "Static");
            this.TreeImageList.Images.SetKeyName(5, "Camera");
            this.TreeImageList.Images.SetKeyName(6, "Resource");
            this.TreeImageList.Images.SetKeyName(7, "Basic");
            this.TreeImageList.Images.SetKeyName(8, "Config");
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.PropertyViewer);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(284, 299);
            this.panel1.TabIndex = 0;
            // 
            // PropertyViewer
            // 
            this.PropertyViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PropertyViewer.Location = new System.Drawing.Point(0, 13);
            this.PropertyViewer.Name = "PropertyViewer";
            this.PropertyViewer.Size = new System.Drawing.Size(282, 284);
            this.PropertyViewer.TabIndex = 0;
            this.PropertyViewer.ToolbarVisible = false;
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.Controls.Add(this.ObjectTypeLbl);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(282, 13);
            this.panel2.TabIndex = 1;
            // 
            // ObjectTypeLbl
            // 
            this.ObjectTypeLbl.AutoSize = true;
            this.ObjectTypeLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ObjectTypeLbl.Location = new System.Drawing.Point(34, 0);
            this.ObjectTypeLbl.Name = "ObjectTypeLbl";
            this.ObjectTypeLbl.Size = new System.Drawing.Size(0, 13);
            this.ObjectTypeLbl.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Type:";
            // 
            // MovableObjectRightClickMenu
            // 
            this.MovableObjectRightClickMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem1});
            this.MovableObjectRightClickMenu.Name = "MovableObjectRightClickMenu";
            this.MovableObjectRightClickMenu.Size = new System.Drawing.Size(117, 26);
            // 
            // deleteToolStripMenuItem1
            // 
            this.deleteToolStripMenuItem1.Name = "deleteToolStripMenuItem1";
            this.deleteToolStripMenuItem1.Size = new System.Drawing.Size(116, 22);
            this.deleteToolStripMenuItem1.Text = "Delete";
            // 
            // MainMenu
            // 
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Size = new System.Drawing.Size(284, 24);
            this.MainMenu.TabIndex = 5;
            this.MainMenu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripMenuItem3,
            this.reloadEngineToolStripMenuItem,
            this.toolStripMenuItem2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.saveAsToolStripMenuItem.Text = "Save as . . .";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(150, 6);
            // 
            // reloadEngineToolStripMenuItem
            // 
            this.reloadEngineToolStripMenuItem.Name = "reloadEngineToolStripMenuItem";
            this.reloadEngineToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.reloadEngineToolStripMenuItem.Text = "Reload engine";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(150, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // SceneEditorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 566);
            this.ControlBox = false;
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.MainMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MainMenuStrip = this.MainMenu;
            this.Name = "SceneEditorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.RenderSystemRightClickMenu.ResumeLayout(false);
            this.SceneNodeRightClickMenu.ResumeLayout(false);
            this.LightsRightClickMenu.ResumeLayout(false);
            this.TerrainConfigRightClickMenu.ResumeLayout(false);
            this.CamerasRightClickMenu.ResumeLayout(false);
            this.ResourcesRightClickMenu.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.MovableObjectRightClickMenu.ResumeLayout(false);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView HierarchyTreeView;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PropertyGrid PropertyViewer;
        private System.Windows.Forms.ImageList TreeImageList;
        private System.Windows.Forms.ContextMenuStrip SceneNodeRightClickMenu;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newSubSceneNodeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AddMovableObjectMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label ObjectTypeLbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem attachedObjectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sceneNodeAndObjectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thisAttachedObjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip MovableObjectRightClickMenu;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip LightsRightClickMenu;
        private System.Windows.Forms.ToolStripMenuItem refreshLightsToolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip CamerasRightClickMenu;
        private System.Windows.Forms.ToolStripMenuItem refreshCamerasToolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip ResourcesRightClickMenu;
        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem reloadEngineToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip RenderSystemRightClickMenu;
        private System.Windows.Forms.ToolStripMenuItem switchRenderSystemToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip TerrainConfigRightClickMenu;
        private System.Windows.Forms.ToolStripMenuItem applyTerrainConfigChangesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reinitializeAllResourcesToolStripMenuItem;
    }
}