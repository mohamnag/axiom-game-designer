﻿namespace GameDesigner
{
    partial class AddNewEntity
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbxGroups = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txbEntityName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cbxMeshList = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.ChkCastShadow = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // cbxGroups
            // 
            this.cbxGroups.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxGroups.FormattingEnabled = true;
            this.cbxGroups.Location = new System.Drawing.Point(106, 12);
            this.cbxGroups.Name = "cbxGroups";
            this.cbxGroups.Size = new System.Drawing.Size(133, 21);
            this.cbxGroups.TabIndex = 22;
            this.cbxGroups.SelectionChangeCommitted += new System.EventHandler(this.cbxGroups_SelectionChangeCommitted);
            this.cbxGroups.DropDown += new System.EventHandler(this.comboBox1_DropDown);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Resource Group:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(164, 115);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txbEntityName
            // 
            this.txbEntityName.Location = new System.Drawing.Point(106, 66);
            this.txbEntityName.Name = "txbEntityName";
            this.txbEntityName.Size = new System.Drawing.Size(133, 20);
            this.txbEntityName.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Entity Name:";
            // 
            // cbxMeshList
            // 
            this.cbxMeshList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxMeshList.Enabled = false;
            this.cbxMeshList.FormattingEnabled = true;
            this.cbxMeshList.Location = new System.Drawing.Point(106, 39);
            this.cbxMeshList.Name = "cbxMeshList";
            this.cbxMeshList.Size = new System.Drawing.Size(133, 21);
            this.cbxMeshList.TabIndex = 15;
            this.cbxMeshList.DropDown += new System.EventHandler(this.cbxMeshList_DropDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(53, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Meshes:";
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(83, 115);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 23;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // ChkCastShadow
            // 
            this.ChkCastShadow.AutoSize = true;
            this.ChkCastShadow.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ChkCastShadow.Location = new System.Drawing.Point(27, 92);
            this.ChkCastShadow.Name = "ChkCastShadow";
            this.ChkCastShadow.Size = new System.Drawing.Size(95, 17);
            this.ChkCastShadow.TabIndex = 24;
            this.ChkCastShadow.Text = "Cast Shadow?";
            this.ChkCastShadow.UseVisualStyleBackColor = true;
            // 
            // AddNewEntity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(247, 148);
            this.Controls.Add(this.ChkCastShadow);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.cbxGroups);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txbEntityName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbxMeshList);
            this.Controls.Add(this.label5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddNewEntity";
            this.Text = "AddNewEntity";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxGroups;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txbEntityName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbxMeshList;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox ChkCastShadow;

    }
}