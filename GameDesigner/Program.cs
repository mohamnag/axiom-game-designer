﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Axiom.Core;

namespace GameDesigner
{
    static class Program
    {
        static internal Root AxiomEngine;

        [STAThread]
        static void Main()
        {
            try
            {

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                PluginManager.LoadPlugins(
                    System.IO.Path.GetDirectoryName(
                        Application.ExecutablePath
                        )
                    );

                using (new LogListener())
                {
                    using (AxiomEngine = new Root(null))
                    {
                        StartUpDialog stdlg = new StartUpDialog();

                        if (stdlg.ShowDialog() == DialogResult.OK)
                        {
                            stdlg.Dispose();

                            using (SceneEditorForm ToolsForm = new SceneEditorForm())
                            {
                                //Setup converters needed for property editing
                                TypeConverters.SetConverters();
                                TypeEditors.SetTypeEditors();

                                ToolsForm.OpenProject(stdlg.EngineDescriptorFile);
                                Application.Run(ToolsForm);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ExceptionReporting.ExceptionReporter reporter = new ExceptionReporting.ExceptionReporter();

                reporter.ReadConfig();  // optionally, read properties from the application's config file

                reporter.Config.ShowSysInfoTab = false;   // alternatively, set properties programmatically
                reporter.Config.ShowFlatButtons = true;   // this particular config is code-only
                reporter.Show(exception);
            }
        }
    }
}
