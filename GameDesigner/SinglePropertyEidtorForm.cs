﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GameDesigner
{
    public partial class SinglePropertyEidtorForm : Form
    {
        public SinglePropertyEidtorForm(bool CancelEnabled)
            : this()
        {
            CancelBtn.Visible = CancelEnabled;
        }

        public SinglePropertyEidtorForm()
        {
            InitializeComponent();
        }
    }
}
