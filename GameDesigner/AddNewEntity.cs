﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Axiom.Core;

namespace GameDesigner
{
    public partial class AddNewEntity : Form
    {
        ResourceGroupManager ResManager;

        public string EntityName
        {
            get
            {
                return txbEntityName.Text;
            }
        }

        public string EntityMesh
        {
            get
            {
                return cbxMeshList.SelectedItem.ToString();
            }
        }

        public bool CastShadow
        {
            get
            {
                return ChkCastShadow.Checked;
            }
        }

        public AddNewEntity(ResourceGroupManager ResManager)
        {
            InitializeComponent();

            this.ResManager = ResManager;
        }

        private void LoadMeshLists()
        {
            cbxMeshList.Items.Clear();

            foreach (string mesh in ResManager.FindResourceNames(cbxGroups.SelectedItem.ToString(), "*.mesh"))
            {
                cbxMeshList.Items.Add(mesh);
            }
        }

        private void cbxMeshList_DropDown(object sender, EventArgs e)
        {
            LoadMeshLists();
        }

        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            LoadGroups();
        }

        private void LoadGroups()
        {
            cbxGroups.Items.Clear();

            foreach (string group in ResManager.GetResourceGroups())
                cbxGroups.Items.Add(group);
        }

        private void cbxGroups_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cbxGroups.SelectedIndex > -1)
                cbxMeshList.Enabled = true;
            else
                cbxMeshList.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cbxMeshList.SelectedIndex < 0 ||
                String.IsNullOrEmpty(txbEntityName.Text))
                MessageBox.Show("Entity name and mesh most be selected.");
            else
                this.DialogResult = DialogResult.OK;
        }
    }
}
