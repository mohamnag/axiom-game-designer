﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GameDesigner
{
    public partial class StartUpDialog : Form
    {
        public string EngineDescriptorFile;

        public StartUpDialog()
        {
            InitializeComponent();
        }

        private void btnNewProjectWizard_Click(object sender, EventArgs e)
        {
            using (StartUpWizard wizardFrm = new StartUpWizard())
            {
                if (wizardFrm.ShowDialog() == DialogResult.OK)
                {
                    EngineDescriptorFile = wizardFrm.EngineDescriptorFile;
                    this.DialogResult = DialogResult.OK;
                }
            }
        }

        private void btnOpenProject_Click(object sender, EventArgs e)
        {
            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "Engine Descriptor (*.xml)|*.xml";
            od.CheckFileExists = true;

            if (od.ShowDialog() == DialogResult.OK)
            {
                EngineDescriptorFile = od.FileName;
                this.DialogResult = DialogResult.OK;
            }
        }
        

    }
}
