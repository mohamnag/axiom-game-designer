﻿namespace GameDesigner
{
    partial class AddResourceDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmBoxResourcesType = new System.Windows.Forms.ComboBox();
            this.cmBoxGroup = new System.Windows.Forms.ComboBox();
            this.txBoxResourceLocation = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.odOpenFile = new System.Windows.Forms.OpenFileDialog();
            this.odOpenFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.chbRecursive = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 191);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Group:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Resource type:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Resource Location:";
            // 
            // cmBoxResourcesType
            // 
            this.cmBoxResourcesType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmBoxResourcesType.FormattingEnabled = true;
            this.cmBoxResourcesType.Items.AddRange(new object[] {
            "ZipFile",
            "Folder"});
            this.cmBoxResourcesType.Location = new System.Drawing.Point(136, 130);
            this.cmBoxResourcesType.Name = "cmBoxResourcesType";
            this.cmBoxResourcesType.Size = new System.Drawing.Size(157, 21);
            this.cmBoxResourcesType.TabIndex = 3;
            // 
            // cmBoxGroup
            // 
            this.cmBoxGroup.FormattingEnabled = true;
            this.cmBoxGroup.Location = new System.Drawing.Point(135, 188);
            this.cmBoxGroup.Name = "cmBoxGroup";
            this.cmBoxGroup.Size = new System.Drawing.Size(157, 21);
            this.cmBoxGroup.TabIndex = 4;
            // 
            // txBoxResourceLocation
            // 
            this.txBoxResourceLocation.Location = new System.Drawing.Point(15, 25);
            this.txBoxResourceLocation.Name = "txBoxResourceLocation";
            this.txBoxResourceLocation.ReadOnly = true;
            this.txBoxResourceLocation.Size = new System.Drawing.Size(277, 20);
            this.txBoxResourceLocation.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(136, 51);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(217, 51);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "Folder";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 294);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(304, 43);
            this.panel2.TabIndex = 8;
            // 
            // button3
            // 
            this.button3.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button3.Location = new System.Drawing.Point(217, 8);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 9;
            this.button3.Text = "Cancel";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button4.Location = new System.Drawing.Point(136, 8);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 8;
            this.button4.Text = "Add";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(304, 1);
            this.panel3.TabIndex = 0;
            // 
            // odOpenFile
            // 
            this.odOpenFile.FileName = "openFileDialog1";
            this.odOpenFile.Filter = "Zip Fiel (*.zip)|*.zip";
            this.odOpenFile.Title = "Select resource file";
            // 
            // odOpenFolder
            // 
            this.odOpenFolder.Description = "Resource Folder";
            // 
            // chbRecursive
            // 
            this.chbRecursive.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbRecursive.Location = new System.Drawing.Point(12, 241);
            this.chbRecursive.Name = "chbRecursive";
            this.chbRecursive.Size = new System.Drawing.Size(180, 24);
            this.chbRecursive.TabIndex = 9;
            this.chbRecursive.Text = "Recursive?";
            this.chbRecursive.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.label16.Location = new System.Drawing.Point(12, 268);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(280, 16);
            this.label16.TabIndex = 23;
            this.label16.Text = "If checked will contain all subdirectories.";
            // 
            // label4
            // 
            this.label4.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.label4.Location = new System.Drawing.Point(12, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(280, 26);
            this.label4.TabIndex = 24;
            this.label4.Text = "Select or type in a group name. Leave empty for General.";
            // 
            // label5
            // 
            this.label5.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.label5.Location = new System.Drawing.Point(13, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(280, 16);
            this.label5.TabIndex = 25;
            this.label5.Text = "Select the typ of resource.";
            // 
            // label6
            // 
            this.label6.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.label6.Location = new System.Drawing.Point(12, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(280, 40);
            this.label6.TabIndex = 26;
            this.label6.Text = "Select a location or a file to add to resources. \r\nBe carefull about the size of " +
                "selected folder and capasity of selected project root.";
            // 
            // AddResourceDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 337);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.chbRecursive);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txBoxResourceLocation);
            this.Controls.Add(this.cmBoxGroup);
            this.Controls.Add(this.cmBoxResourcesType);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "AddResourceDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AddResourceDialog";
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmBoxResourcesType;
        private System.Windows.Forms.ComboBox cmBoxGroup;
        private System.Windows.Forms.TextBox txBoxResourceLocation;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.OpenFileDialog odOpenFile;
        private System.Windows.Forms.FolderBrowserDialog odOpenFolder;
        private System.Windows.Forms.CheckBox chbRecursive;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}