﻿namespace GameDesigner
{
    partial class openFileControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openButton = new System.Windows.Forms.Button();
            this.txbAddress = new System.Windows.Forms.TextBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // openButton
            // 
            this.openButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.openButton.Location = new System.Drawing.Point(259, 0);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(29, 23);
            this.openButton.TabIndex = 3;
            this.openButton.Text = "...";
            this.openButton.UseVisualStyleBackColor = true;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // txbAddress
            // 
            this.txbAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txbAddress.Location = new System.Drawing.Point(0, 0);
            this.txbAddress.Name = "txbAddress";
            this.txbAddress.Size = new System.Drawing.Size(259, 20);
            this.txbAddress.TabIndex = 2;
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // openFileControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txbAddress);
            this.Controls.Add(this.openButton);
            this.Name = "openFileControl";
            this.Size = new System.Drawing.Size(288, 23);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.TextBox txbAddress;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
    }
}
