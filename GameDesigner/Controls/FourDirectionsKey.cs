﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GameDesigner
{
    public partial class FourDirectionsKey : UserControl
    {
        [Category("Direction Click")]
        [Description("Fires when the Up button is clicked.")]
        public event EventHandler UpButtonClick
        {
            add
            {
                UpButton.Click += value;
            }
            remove
            {
                UpButton.Click -= value;
            }
        }

        [Category("Direction Click")]
        [Description("Fires when the Down button is clicked.")]
        public event EventHandler DownButtonClick
        {
            add
            {
                DownButton.Click += value;
            }
            remove
            {
                DownButton.Click -= value;
            }
        }

        [Category("Direction Click")]
        [Description("Fires when the Left button is clicked.")]
        public event EventHandler LeftButtonClick
        {
            add
            {
                LeftButton.Click += value;
            }
            remove
            {
                LeftButton.Click -= value;
            }
        }

        [Category("Direction Click")]
        [Description("Fires when the Right button is clicked.")]
        public event EventHandler RightButtonClick
        {
            add
            {
                RightButton.Click += value;
            }
            remove
            {
                RightButton.Click -= value;
            }
        }

        public FourDirectionsKey()
        {
            InitializeComponent();

        }

    }
}
