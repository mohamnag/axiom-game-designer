﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GameDesigner
{
    public partial class openFileControl : UserControl
    {
        public bool ReadOnly
        {
            set
            {
                txbAddress.ReadOnly = value;
            }
            get
            {
                return txbAddress.ReadOnly;
            }
        }

        public override string Text
        {
            set
            {
                txbAddress.Text = value;
            }
            get
            {
                return txbAddress.Text;
            }
        }

        public openFileControl()
        {
            InitializeComponent();
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
                txbAddress.Text = openFileDialog.FileName;
        }

    }
}
