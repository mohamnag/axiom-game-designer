﻿namespace GameDesigner
{
    partial class FourDirectionsKey
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.UpButton = new GameDesigner.RoundButton();
            this.RightButton = new GameDesigner.RoundButton();
            this.DownButton = new GameDesigner.RoundButton();
            this.LeftButton = new GameDesigner.RoundButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GameDesigner.Properties.Resources.xbox_controller_s1;
            this.pictureBox1.Location = new System.Drawing.Point(-2, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(94, 91);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // UpButton
            // 
            this.UpButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UpButton.Location = new System.Drawing.Point(0, 0);
            this.UpButton.Name = "UpButton";
            this.UpButton.Size = new System.Drawing.Size(90, 90);
            this.UpButton.StartAngle = 225F;
            this.UpButton.SweepAngle = 90F;
            this.UpButton.TabIndex = 7;
            this.UpButton.Text = "roundButton4";
            this.UpButton.UseVisualStyleBackColor = true;
            // 
            // RightButton
            // 
            this.RightButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RightButton.Location = new System.Drawing.Point(0, 0);
            this.RightButton.Name = "RightButton";
            this.RightButton.Size = new System.Drawing.Size(90, 90);
            this.RightButton.StartAngle = -45F;
            this.RightButton.SweepAngle = 90F;
            this.RightButton.TabIndex = 6;
            this.RightButton.Text = "roundButton3";
            this.RightButton.UseVisualStyleBackColor = true;
            // 
            // DownButton
            // 
            this.DownButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DownButton.Location = new System.Drawing.Point(0, 0);
            this.DownButton.Name = "DownButton";
            this.DownButton.Size = new System.Drawing.Size(90, 90);
            this.DownButton.StartAngle = 45F;
            this.DownButton.SweepAngle = 90F;
            this.DownButton.TabIndex = 5;
            this.DownButton.Text = "roundButton2";
            this.DownButton.UseVisualStyleBackColor = true;
            // 
            // LeftButton
            // 
            this.LeftButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LeftButton.Location = new System.Drawing.Point(0, 0);
            this.LeftButton.Name = "LeftButton";
            this.LeftButton.Size = new System.Drawing.Size(90, 90);
            this.LeftButton.StartAngle = 135F;
            this.LeftButton.SweepAngle = 90F;
            this.LeftButton.TabIndex = 4;
            this.LeftButton.Text = "roundButton1";
            this.LeftButton.UseVisualStyleBackColor = true;
            // 
            // FourDirectionsKey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.UpButton);
            this.Controls.Add(this.RightButton);
            this.Controls.Add(this.DownButton);
            this.Controls.Add(this.LeftButton);
            this.Controls.Add(this.pictureBox1);
            this.Name = "FourDirectionsKey";
            this.Size = new System.Drawing.Size(90, 90);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private RoundButton UpButton;
        private RoundButton RightButton;
        private RoundButton DownButton;
        private RoundButton LeftButton;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
