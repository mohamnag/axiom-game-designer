﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GameDesigner
{
    public partial class openDialog : UserControl
    {
        public bool ReadOnly
        {
            set
            {
                txbAddress.ReadOnly = value;
            }
            get
            {
                return txbAddress.ReadOnly;
            }
        }

        public override string Text
        {
            set
            {
                txbAddress.Text = value;
            }
            get
            {
                return txbAddress.Text;
            }
        }

        public openDialog()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
                txbAddress.Text = folderBrowserDialog.SelectedPath;
        }
    }
}
