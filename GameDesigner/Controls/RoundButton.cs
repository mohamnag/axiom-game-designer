﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace GameDesigner
{
    public partial class RoundButton : Button
    {
        public float StartAngle { set; get; }
        public float SweepAngle { set; get; }

        public RoundButton()
        {
            InitializeComponent();

            StartAngle = 135f;
            SweepAngle = 90f;

            this.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            Rectangle newRectangle = ClientRectangle;

            Graphics g = pe.Graphics;
            g.FillRectangle(Brushes.Transparent, newRectangle);

            GraphicsPath BorderPath = new GraphicsPath();
            BorderPath.AddPie(newRectangle, StartAngle, SweepAngle);
            BorderPath.AddLine(BorderPath.GetLastPoint(),
                new PointF(newRectangle.Width / 2f, newRectangle.Height / 2f));
            BorderPath.CloseFigure();

            this.Region = new Region(BorderPath);

            //base.OnPaint(pevent);
        }

        //protected override void OnPaint(PaintEventArgs pevent)
        //{
        //    GraphicsPath buttonPath = new GraphicsPath();

        //    Rectangle newRectangle = ClientRectangle;

        //    // Decrease the size of the rectangle.
        //    newRectangle.Inflate(-5, -5);

        //    // Draw the button's border.
        //    //pevent.Graphics.DrawArc(System.Drawing.Pens.Black, newRectangle, 135, 225);

        //    // Increase the size of the rectangle to include the border.
        //    //newRectangle.Inflate( 1,  1);

        //    // Create a circle within the new rectangle.
        //    buttonPath.AddArc(newRectangle, 135, 225);
        //    //pevent.Graphics.DrawPath(new Pen(Color.Red, 3), buttonPath);

        //    // Set the button's Region property to the newly created 
        //    // circle region.
        //    this.Region = new System.Drawing.Region(buttonPath);


        //    base.OnPaint(pevent);
        //}
    }
}
