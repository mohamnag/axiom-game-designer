﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Axiom.Core;

namespace GameDesigner
{
    public partial class ResourceListEditor : Form
    {
        ResourceList list;

        public ResourceListEditor(ResourceList list)
        {
            InitializeComponent();

            this.list = list;
            RefreshListBox();
        }

        public List<Resource> getList()
        {
            return list.Resources;
        }

        private void RefreshListBox()
        {
            ResourcesListBox.Items.Clear();

            foreach (Resource res in list.Resources)
                ResourcesListBox.Items.Add(res);
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            Resource newRes = new Resource(list.RootFolder.FullName);
            SinglePropertyEidtorForm edFrm = new SinglePropertyEidtorForm(true);
            edFrm.EditorPG.SelectedObject = newRes;

            if (edFrm.ShowDialog() == DialogResult.OK)
            {
                list.AddResource(newRes);
                ResourceGroupManager.Instance.AddResourceLocation(newRes.Location, newRes.Type, newRes.Group, newRes.Recursive, false);
                ResourceGroupManager.Instance.InitializeResourceGroup(newRes.Group);
                RefreshListBox();
            }
        }

        private void RemoveBtn_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("This will delete all current files of this resource. Are you sure?", "Attention", MessageBoxButtons.YesNo) ==
                 DialogResult.Yes)
            {
                Resource rmvRes = (Resource)ResourcesListBox.SelectedItem;
                list.RemoveResource(rmvRes);
                ResourceGroupManager.Instance.RemoveResourceLocation(rmvRes.Location, rmvRes.Group);
                ResourceGroupManager.Instance.InitializeResourceGroup(rmvRes.Group);
                RefreshListBox();
            }
        }

        private void ResourcesListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            RemoveBtn.Enabled = ((ListBox)sender).SelectedIndex >= 0;
        }

    }
}
