﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Design;
using System.Windows.Forms;
using System.IO;
using System.ComponentModel;

namespace GameDesigner
{
    class TypeEditors
    {
        protected static void RegisterTypeEditor<T, TC>() where TC : UITypeEditor
        {
            Attribute[] attr = new Attribute[1];
            EditorAttribute vConv = new EditorAttribute(typeof(TC), typeof(UITypeEditor));
            attr[0] = vConv;
            TypeDescriptor.AddAttributes(typeof(T), attr);
        }

        public static void SetTypeEditors()
        {
            RegisterTypeEditor<Axiom.Graphics.Collections.ConfigOptionCollection, ConfigOptionCollectionTypeEditor>();
        }

        public class ResourceListTypeEditor : UITypeEditor
        {
            public override UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
            {
                return UITypeEditorEditStyle.Modal;
            }

            public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, IServiceProvider provider, object value)
            {
                if (value is List<Resource>)
                {
                    ResourceListEditor resListEditor = new ResourceListEditor((ResourceList)context.Instance);
                    resListEditor.ShowDialog();

                    return resListEditor.getList();
                }

                return base.EditValue(context, provider, value);
            }
        }

        public class ResourceLocationTypeEditor : UITypeEditor
        {
            public override UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
            {
                return UITypeEditorEditStyle.Modal;
            }

            public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, IServiceProvider provider, object value)
            {
                if (context.Instance is Resource)
                {
                    Resource res = (Resource)context.Instance;

                    if (res.Type.Equals("ZipFile"))
                    {
                        OpenFileDialog od = new OpenFileDialog();
                        od.Filter = "Zip files (*.zip)|*.zip";
                        od.CheckFileExists = true;

                        if (od.ShowDialog() == DialogResult.OK)
                        {
                            return od.FileName;
                        }
                    }
                    else if (res.Type.Equals("Folder"))
                    {
                        FolderBrowserDialog od = new FolderBrowserDialog();
                        if (od.ShowDialog() == DialogResult.OK)
                        {
                            return od.SelectedPath;
                        }
                    }
                }

                return base.EditValue(context, provider, value);
            }
        }

        public class ConfigOptionCollectionTypeEditor : UITypeEditor
        {
            public override UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
            {
                return UITypeEditorEditStyle.Modal;
            }

            public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, IServiceProvider provider, object value)
            {
                if (context.Instance is Axiom.Graphics.RenderSystem)
                {
                    Axiom.Graphics.RenderSystem renderSystem = (Axiom.Graphics.RenderSystem)context.Instance;

                    ConfigOptionCollectionEditorForm eFrm = new ConfigOptionCollectionEditorForm(
                        (Axiom.Graphics.Collections.ConfigOptionCollection)value);

                    eFrm.ShowDialog();
                }

                return base.EditValue(context, provider, value);
            }
        }

        public class OpenFileTypeEditor : UITypeEditor
        {

            public override UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
            {
                return UITypeEditorEditStyle.Modal;
            }

            public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, IServiceProvider provider, object value)
            {

                OpenFileDialog od = new OpenFileDialog();
                od.Filter = "All files (*.*)|*.*";
                od.CheckFileExists = true;
                od.FileName = (string)value;

                if (od.ShowDialog() == DialogResult.OK)
                {
                    if (context.Instance is TerrainSceneManagerTerrainConfig)
                    {
                        ResourceList resList = ((TerrainSceneManagerTerrainConfig)context.Instance).ResourceList;
                        if (value != null)
                            resList.RemoveFileFromGeneral((string)value);
                        resList.AddFileToGeneral(od.FileName);
                    }

                    return Path.GetFileName(od.FileName);
                }
                else
                {
                    return value;
                }

            }

        }
    }
}
