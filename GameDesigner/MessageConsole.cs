﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GameDesigner
{
    public partial class MessageConsole : Form
    {

        public MessageConsole()
        {
            InitializeComponent();

            this.Left = 0;
            this.Top = Screen.PrimaryScreen.WorkingArea.Height - this.Bounds.Height;
            this.Width = Screen.PrimaryScreen.WorkingArea.Width;
        }

        public void AddLine(string Message, DateTime Time)
        {
            try
            {
                txbMessages.AppendText("\n[" + Time.ToLongTimeString() + "] " + Message);
                this.Refresh();
            }
            catch
            { }
        }

        public void AddLine(string Message)
        {
            AddLine(Message, DateTime.Now);
        }

        private void txbMessages_TextChanged(object sender, EventArgs e)
        {
            ((RichTextBox)sender).ScrollToCaret();
        }

    }

    public class LogListener : System.Diagnostics.TraceListener
    {
        MessageConsole OutputConsole;

        public override void Write(string message)
        {
            OutputConsole.AddLine(message);
        }

        public override void WriteLine(string message)
        {
            OutputConsole.AddLine(message);
        }

        public LogListener()
        {
            OutputConsole = new MessageConsole();
            OutputConsole.txbMessages.Text = "*** Log Console initiated and hooked into system debug! ***";
            OutputConsole.Show();

            System.Diagnostics.Debug.Listeners.Add(this);
        }
    }
}
