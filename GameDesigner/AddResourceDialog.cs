﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GameDesigner
{
    public partial class AddResourceDialog : Form
    {
        public AddResourceDialog()
        {
            InitializeComponent();

            cmBoxResourcesType.SelectedItem = 0;
        }

        public void loadGroups(List<string> Groups)
        {
            cmBoxGroup.Items.Clear();

            foreach (string group in Groups)
                cmBoxGroup.Items.Add(group);
        }

        public Resource getResource()
        {
            return new Resource(
                txBoxResourceLocation.Text,
                cmBoxResourcesType.SelectedItem.ToString(),
                !String.IsNullOrEmpty(cmBoxGroup.Text) ? cmBoxGroup.Text : "General",
                chbRecursive.Checked
                );

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (odOpenFile.ShowDialog() == DialogResult.OK)
                txBoxResourceLocation.Text = odOpenFile.FileName;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (odOpenFolder.ShowDialog() == DialogResult.OK)
                txBoxResourceLocation.Text = odOpenFolder.SelectedPath;
        }
    }
}
